package com.abans.webapi.service;

import com.abans.webapi.dto.OrdersDto;

public interface OrderService {
    public boolean save(OrdersDto ordersDto) throws Exception;
}

package com.abans.webapi.service.impl;

import com.abans.webapi.dao.CategoryDao;
import com.abans.webapi.dao.ProductDao;
import com.abans.webapi.dao.SubCategoryDao;
import com.abans.webapi.dto.CategoryDto;
import com.abans.webapi.dto.CategoryProductDto;
import com.abans.webapi.dto.ProductDto;
import com.abans.webapi.dto.SubCategoryDto;
import com.abans.webapi.model.Category;
import com.abans.webapi.model.Product;
import com.abans.webapi.model.SubCategory;
import com.abans.webapi.service.CategoryService;
import com.abans.webapi.service.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Transactional
@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;
    @Autowired
    private CategoryDao categoryDao;
    @Autowired
    private SubCategoryDao subCategoryDao;
    @Autowired
    private ModelMapper mapper;

    @Override
    public List<ProductDto> readAllBySubCategoryName(String name) throws Exception {
        List<Product> products = productDao.readAllBySubCategoryName(name);

        return products.stream()
                .map(this::getProductDto)
                .collect(Collectors.toList());
    }

    @Override
    public ProductDto findById(String id) throws Exception {
        ProductDto productDto = getProductDto(productDao.findById(id));

        SubCategory subCategory = subCategoryDao.findById(productDto.getSubCategoryId());

        productDto.setCategoryName(categoryDao.findById(subCategory.getCategoryId()).getName());
        productDto.setSubCategoryName(subCategory.getName());

        return productDto;
    }

    @Override
    public List<CategoryProductDto> readAllByCategory() throws Exception {
        List<CategoryProductDto> categoryProductDtoList = new ArrayList<>();

        List<Category> categories = categoryDao.readAll();

        for (Category category:categories) {
            String categoryId = category.getId();

            List<Product> products = productDao.readAllByCategoryId(categoryId);

            List<ProductDto> productDtos = products.stream()
                    .map(this::getProductDto)
                    .collect(Collectors.toList());

            if(!productDtos.isEmpty()){
                CategoryProductDto categoryProductDto = new CategoryProductDto();

                categoryProductDto.setCategory(getCategoryDto(category));
                categoryProductDto.setProducts(productDtos);

                categoryProductDtoList.add(categoryProductDto);
            }

        }

        return categoryProductDtoList;
    }

    private ProductDto getProductDto(Product product) {
        ProductDto productDto = mapper.map(product, ProductDto.class);
        return productDto;
    }

    private CategoryDto getCategoryDto(Category category) {
        CategoryDto categoryDto = mapper.map(category, CategoryDto.class);
        return categoryDto;
    }
}

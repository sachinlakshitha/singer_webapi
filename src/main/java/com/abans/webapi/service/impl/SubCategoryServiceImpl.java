package com.abans.webapi.service.impl;

import com.abans.webapi.dao.SubCategoryDao;
import com.abans.webapi.dto.SubCategoryDto;
import com.abans.webapi.service.SubCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
@Service
public class SubCategoryServiceImpl implements SubCategoryService {
    @Autowired
    private SubCategoryDao subCategoryDao;

    @Override
    public List<SubCategoryDto> readAll() throws Exception {
        return null;
    }
}

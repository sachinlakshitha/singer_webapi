package com.abans.webapi.service;

import com.abans.webapi.dto.CategoryDto;
import com.abans.webapi.model.Category;

import java.util.List;

public interface CategoryService {
    public List<CategoryDto> readAll() throws Exception;
}

package com.abans.webapi.dto;

import java.util.List;

public class CategoryProductDto {
    private CategoryDto category;
    private List<ProductDto> products;

    public CategoryDto getCategory() {
        return category;
    }

    public void setCategory(CategoryDto category) {
        this.category = category;
    }

    public List<ProductDto> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDto> products) {
        this.products = products;
    }
}

package com.abans.webapi.dao;

import com.abans.webapi.model.OrderProduct;

public interface OrderProductDao {
    public int save(OrderProduct orderProduct) throws Exception;
}

package com.abans.webapi.dao;

import com.abans.webapi.model.Product;

import java.util.List;

public interface ProductDao {
    public List<Product> readAllBySubCategoryName(String name) throws Exception;
    public Product findById(String id) throws Exception;
    public List<Product> readAllByCategoryId(String categoryId) throws Exception;
}

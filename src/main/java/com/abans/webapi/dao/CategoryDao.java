package com.abans.webapi.dao;

import com.abans.webapi.model.Category;

import java.util.List;

public interface CategoryDao {
    public List<Category> readAll() throws Exception;
    public Category findById(String id) throws Exception;
}

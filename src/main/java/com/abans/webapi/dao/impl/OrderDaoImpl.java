package com.abans.webapi.dao.impl;

import com.abans.webapi.dao.OrderDao;
import com.abans.webapi.model.Orders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@Repository
public class OrderDaoImpl implements OrderDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Orders orders) throws Exception {
        String sql = "INSERT INTO ORDERS (ORDER_ID,BILLING_FIRSTNAME,BILLING_LASTNAME,BILLING_ADDRESS," +
                "BILLING_CITY,BILLING_POSTAL_CODE,BILLING_PHONE_NUMBER,BILLING_EMAIL,DELIVERY_FIRSTNAME,DELIVERY_LASTNAME," +
                "DELIVERY_ADDRESS,DELIVERY_CITY,DELIVERY_POSTAL_CODE,DELIVERY_PHONE_NUMBER,DELIVERY_EMAIL,NOTES,AMOUNT) VALUES " +
                "(:id,:billingfirstname,:billinglastname,:billingaddress,:billingcity,:billingpostalcode," +
                ":billingphonenumber,:billingemail,:deliveryfirstname,:deliverylastname,:deliveryaddress,:deliverycity," +
                ":deliverypostalcode,:deliveryphonenumber,:deliveryemail,:notes,:amount)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", orders.getId());
        mapSqlParameterSource.addValue("billingfirstname", orders.getBillingFirstName());
        mapSqlParameterSource.addValue("billinglastname", orders.getBillingLastName());
        mapSqlParameterSource.addValue("billingaddress", orders.getBillingAddress());
        mapSqlParameterSource.addValue("billingcity", orders.getBillingCity());
        mapSqlParameterSource.addValue("billingpostalcode", orders.getBillingPostalCode());
        mapSqlParameterSource.addValue("billingphonenumber", orders.getBillingPhoneNumber());
        mapSqlParameterSource.addValue("billingemail", orders.getBillingEmail());
        mapSqlParameterSource.addValue("deliveryfirstname", orders.getDeliveryFirstName());
        mapSqlParameterSource.addValue("deliverylastname", orders.getDeliveryLastName());
        mapSqlParameterSource.addValue("deliveryaddress", orders.getDeliveryAddress());
        mapSqlParameterSource.addValue("deliverycity", orders.getDeliveryCity());
        mapSqlParameterSource.addValue("deliverypostalcode", orders.getDeliveryPostalCode());
        mapSqlParameterSource.addValue("deliveryphonenumber", orders.getDeliveryPhoneNumber());
        mapSqlParameterSource.addValue("deliveryemail", orders.getDeliveryEmail());
        mapSqlParameterSource.addValue("notes", orders.getNotes());
        mapSqlParameterSource.addValue("amount", orders.getAmount());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

}

package com.abans.webapi.dao.impl;

import com.abans.webapi.dao.PaymentDao;
import com.abans.webapi.model.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class PaymentDaoImpl implements PaymentDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int save(Payment payment) throws Exception {
        String sql = "INSERT INTO PAYMENT (PAYMENT_ID,ORDER_ID,PAYMENT_METHOD,AMOUNT,CARD_OWNER," +
                "CARD_NUMBER,EXPIRY_DATE,CVV_CODE) VALUES (:id,:orderid,:paymentmethod,:amount,:owner," +
                ":number,:expirydate,:cvvcode)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", payment.getId());
        mapSqlParameterSource.addValue("orderid", payment.getOrderId());
        mapSqlParameterSource.addValue("paymentmethod", payment.getPaymentMethod());
        mapSqlParameterSource.addValue("amount", payment.getAmount());
        mapSqlParameterSource.addValue("owner", payment.getCardOwner());
        mapSqlParameterSource.addValue("number", payment.getCardNumber());
        mapSqlParameterSource.addValue("expirydate", payment.getExpiryDate());
        mapSqlParameterSource.addValue("cvvcode", payment.getCvvCode());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }
}

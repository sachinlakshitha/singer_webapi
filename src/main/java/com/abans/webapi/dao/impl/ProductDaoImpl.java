package com.abans.webapi.dao.impl;

import com.abans.webapi.dao.ProductDao;
import com.abans.webapi.model.Product;
import com.abans.webapi.model.SubCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductDaoImpl implements ProductDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Product> readAllBySubCategoryName(String name) throws Exception {
        String sql = "SELECT P.* FROM PRODUCT P,SUB_CATEGORY SC WHERE P.SUB_CATEGORY_ID=SC.SUB_CATEGORY_ID AND SC.SUB_CATEGORY_NAME=?";

        List<Product> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Product>>() {

                @Override
                public List<Product> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Product> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Product product = getProduct(resultSet);
                            listTemp.add(product);

                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },name);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return list;
    }

    @Override
    public Product findById(String id) throws Exception {
        String sql = "SELECT * FROM PRODUCT WHERE PRODUCT_ID=?";

        List<Product> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Product>>() {

                @Override
                public List<Product> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Product> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Product product = getProduct(resultSet);
                            listTemp.add(product);

                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },id);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    public List<Product> readAllByCategoryId(String categoryId) throws Exception {
        String sql = "SELECT P.* FROM PRODUCT P,SUB_CATEGORY SP,CATEGORY C WHERE P.SUB_CATEGORY_ID=SP.SUB_CATEGORY_ID AND SP.CATEGORY_ID=C.CATEGORY_ID AND C.CATEGORY_ID=? LIMIT 4";

        List<Product> list = null;

        try {
            list = jdbcTemplate.query(sql, new ResultSetExtractor<List<Product>>() {

                @Override
                public List<Product> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Product> listTemp = new ArrayList<>();

                    while (resultSet.next()) {
                        try {
                            Product product = getProduct(resultSet);
                            listTemp.add(product);

                        } catch (Exception ex) {

                        }
                    }

                    return listTemp;
                }
            },categoryId);

        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;

        } catch (Exception exception) {

        }

        return list;
    }

    private Product getProduct(ResultSet resultSet) throws SQLException, Exception {
        Product product = new Product();

        product.setId(resultSet.getString("PRODUCT_ID"));
        product.setSubCategoryId(resultSet.getString("SUB_CATEGORY_ID"));
        product.setBrandId(resultSet.getString("BRAND_ID"));
        product.setProductCode(resultSet.getString("PRODUCT_CODE"));
        product.setModelNo(resultSet.getString("MODEL_NO"));
        product.setName(resultSet.getString("NAME"));
        product.setDescription(resultSet.getString("DESCRIPTION"));
        product.setRating(resultSet.getString("RATING"));
        product.setImage(resultSet.getString("IMAGE"));
        product.setPrice(resultSet.getDouble("PRICE"));
        product.setDiscount(resultSet.getDouble("DISCOUNT"));
        product.setWarranty(resultSet.getString("WARRANTY"));
        product.setAvailability(resultSet.getInt("AVAILABILITY"));

        return product;
    }
}
